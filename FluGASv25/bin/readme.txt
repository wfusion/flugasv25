﻿
## FluGAS で利用するWindowsで実行する外部ソフト
https://drive.google.com/open?id=1gbI3vlcIVIOH0xBT-91Gl7gqk5XGyYML

bowtie2
GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007
Ben Langmead <langmea@cs.jhu.edu> 
MiSeq シーケンサから出力されたFASTQのマッピング

cd-hit
GNU GENERAL PUBLIC LICENSE Version 2, June 1991
Dr. Weizhong Li(J Craig Venter Institute)
参照配列の更新時に相同性の高い配列を統合する


graphmap 廃止
The MIT License (MIT)
Ivan Sovic <ivan.sovic@irb.hr>
MiIONシーケンサから出力されたFASTQのマッピング

IGV
The MIT License (MIT)
Broad Institute
最終マッピングのアライメント状態をみる

minimap2
The MIT License (MIT)
Heng Li
Citations/ Li, H. (2018). Minimap2: pairwise alignment for nucleotide sequences. Bioinformatics, 34:3094-3100. doi:10.1093/bioinformatics/bty191
MiIONシーケンサから出力されたFASTQのマッピング

ncbi-blast
GNU LESSER GENERAL PUBLIC LICENSE Version 2.1, February 1999
National Library of Medicine
National Center for Biotechnology Information, U.S. National Library of Medicine
ウイルス型判定を行うための相同性検索

samtools
The MIT/Expat License
Heng Li
マッピングされたSAMファイルの操作

Trimmomatic
GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007
Bolger Anthony
Citations/Bolger, A. M., Lohse, M., & Usadel, B. (2014). Trimmomatic: A flexible trimmer for Illumina Sequence Data. Bioinformatics, btu170.
MiSeqシーケンサから出力されたFASTQのトリミング

Porechop 廃止
GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007
Ryan Wick
MinIONシーケンサから出力されたFASTQのバーコード分割

poretools 廃止
MIT License
Aaron Quinlan
MinION Fast5 形式からからFASTQを得る
