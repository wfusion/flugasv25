﻿@echo off

rem FluGAS付属品
rem カレントディレクトリにある .fna ファイルに対して、createdatabase コマンドを実行します。
cd %~dp0

set BIN=..\..\bin\ncbi-blast\bin\makeblastdb.exe
%BIN%


%BIN% -in  Victoria.fna -dbtype nucl -parse_seqids  -out  Victoria
%BIN% -in  Yamagata.fna -dbtype nucl -parse_seqids  -out  Yamagata

pause
